import React, {useState, createContext, useEffect,useReducer} from "react";
import axios from "axios"


export const GlobalContext = createContext();

export const GlobalProvider = (props) => {
    //ids reducer
    const initialState ={
        ids: []
    }
    const reducer =(state,action) => {
        switch(action.type){
            case "ADD_ID":
                return {
                    ids: [...state.ids, action.id]
                }
            case "REMOVE_ID":
                if (state.ids.length>0){
                    return {
                        ids: [
                            ...state.ids.filter(el =>{
                                return el !== action.id
                            })
                        ]
                    }
                }
            default:
                return state
        }
    }
    const [store,dispatch] = useReducer(reducer, initialState)
    // range reducer
    const rangeInitialState = {
        range: 0
    }
    const rangeReducer =(state,action) => {
        switch(action.type){
            case "NEXT_RANGE":
                    return {
                        range: state.range+action.range
                    }
            case "PREV_RANGE":
                    return {
                        range: state.range-action.range
                    }
            default:
                return state
        }
    }
    const [rangeStore,rangeDispatch] = useReducer(rangeReducer, rangeInitialState)
    // states
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);
    const [name, setName] = useState("");
    const [rangeJump, setRangeJump] = useState(25)
    
    // fetching data
    const url = "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json";
    useEffect(()=>{
        axios
        .get(url)
        .then((res)=>{
          if (res.data.length !== 0) {
              setUsers(res.data)
              setLoading(false)
          } else {
              alert("Response has no data")
              setLoading(false)
          }
        })
        .catch(err=>{
          console.log(err)
        })
    },[]);
    
return (
    <GlobalContext.Provider value={{rangeStore, rangeDispatch, store,dispatch, rangeJump, setRangeJump, users, setUsers, loading, setLoading, name, setName}}>
        {props.children}
    </GlobalContext.Provider>
    )
}
