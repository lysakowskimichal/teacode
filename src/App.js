import React from 'react';
import { Container } from "react-bootstrap";
import {GlobalProvider} from "./Context"
import ContacList from "./ContactList"
import UsersFilter from "./UsersFilter"

const App = () => {
  return (
    <GlobalProvider>
      <Container fluid>
        <UsersFilter/>
        <ContacList/>
      </Container>
    </GlobalProvider>
  );
}

export default App;
