import React,{useContext, useEffect} from 'react';
import {GlobalContext} from "../Context"
import {ListGroup} from "react-bootstrap"
import ControlPanel from "../ControlPanel"
import UserItem from "../UserItem"
import LoadingPlaceholder from "../LoadingPlaceholder"

const ContactList = () => {
    const {loading,users,rangeStore,name,store,rangeJump} = useContext(GlobalContext)
    const rangeOfUsers = rangeStore.range+rangeJump
    const sortedUsers = users.sort((a,b)=> a.last_name.localeCompare(b.last_name))
    const filteredUsers = sortedUsers.filter(user => user.first_name.toLowerCase().indexOf(name) > -1 || user.last_name.toLowerCase().indexOf(name) > -1 || user.first_name.indexOf(name) > -1 || user.last_name.indexOf(name) > -1 || user.first_name.toUpperCase().indexOf(name) > -1 || user.last_name.toUpperCase().indexOf(name) > -1)
    const userList = filteredUsers.slice(rangeStore.range,rangeOfUsers).map((user) => <UserItem key={user.id} user={user}/>)
    
    useEffect(()=>{
        return console.log(store.ids)
    },[store.ids])
    
    return (
        <>
            {loading ? (
                <LoadingPlaceholder/>   
            ):(
                <ListGroup>
                   {userList}
                   <ControlPanel/>
                </ListGroup>
            )}
        </>
    )
}
 
export default ContactList;