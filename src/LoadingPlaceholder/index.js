import React from 'react';
import {Spinner,Row} from 'react-bootstrap'

const LoadingPlaceholder = () => {
    return (
        <Row style={{justifyContent: "center"}}>
            <Spinner animation="grow" variant="primary"/>
        </Row> 
     );
}
 
export default LoadingPlaceholder;