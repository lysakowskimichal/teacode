import React,{useContext} from 'react';
import {ListGroup,InputGroup} from "react-bootstrap"
import {Row, Col, Image} from "react-bootstrap"
import { GlobalContext } from '../Context';

const UserItem = ({user}) => {
    const { avatar, first_name,last_name,id} = user
    const { dispatch, store } = useContext(GlobalContext)
    const handleCheck = () => {
        if(!store.ids.includes(id)){
            dispatch({type: "ADD_ID", id})
        } else if (store.ids.includes(id)) {
            dispatch({type: "REMOVE_ID", id})
        }
    }
    return ( 
        <ListGroup.Item>
            <Row>
            <Col xl={2} md={2} style={{textAlign: "center"}}>
            {avatar !== null ? <Image src={avatar} roundedCircle /> : <Image src="https://i.pravatar.cc/50/50" roundedCircle />}
            </Col>
            <Col xl={9} md={9}>
                {first_name} {last_name}
            </Col>
            <Col xl={1} md={1}>
                <InputGroup className="mb-3">
                    <InputGroup>
                        <InputGroup.Checkbox onChange={handleCheck} aria-label="Checkbox for following text input" />
                    </InputGroup>
                </InputGroup>
            </Col>
            </Row>
        </ListGroup.Item>
    );
}
 
export default UserItem;