import React,{useContext} from 'react';
import {Button, Row, Col,Form, ListGroup} from "react-bootstrap"
import { GlobalContext } from '../Context';
const ControlPanel = () => {
    const {users,rangeStore, rangeDispatch,rangeJump, setRangeJump} = useContext(GlobalContext)

    const handlePrev = () =>{
        if(rangeStore.range>=rangeJump) return rangeDispatch({type: "PREV_RANGE", range: rangeJump})
    }
    const handleNext = () => {
        if (rangeStore.range<(users.length-rangeJump)) return rangeDispatch({type: "NEXT_RANGE", range: rangeJump})       
    }
    const handleJump = (e) => {
        setRangeJump(e.target.value)
    }
    const centerLayout = {textAlign:"center",alignSelf: "center"}
    return ( 
        <ListGroup>
            <Row>
                <Col xl={3} sm={3} style={centerLayout}>
                    <Button onClick={handlePrev}>{"<< Prev"}</Button>
                </Col>
                <Col xl={2} sm={2} style={centerLayout}>
                    {rangeStore.range+1} - {rangeStore.range+26}
                </Col>
                <Col xl={4} sm={4} style={centerLayout}>
                <Form.Group style={{display: "flex",alignItems: "center",justifyContent: "space-evenly"}}>
                    <Form.Label style={{textAlign:"left",alignSelf: "center"}}>Set range jump</Form.Label>
                    <Form.Control style={{width:"150px"}} type="number" value={rangeJump} min={10} onChange={handleJump}/>
                </Form.Group>
                </Col>
                <Col xl={3} sm={3} style={centerLayout}>
                    <Button onClick={handleNext}>{"Next >>"}</Button>
                </Col>
            </Row>
        </ListGroup>
     );
}
 
export default ControlPanel;