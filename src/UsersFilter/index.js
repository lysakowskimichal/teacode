import React,{useEffect, useContext} from 'react';
import { GlobalContext } from '../Context';
import { Row, InputGroup,FormControl } from "react-bootstrap";

const UsersFilter = () => {
    const {name, setName} = useContext(GlobalContext) 

    const handleChange = (e) => {
        setName(e.target.value)
    }
    return ( 
        <>
            <Row style={{justifyContent: "center"}}>
                <h3>
                 Contact List
                </h3>
            </Row>
            <Row>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text>User</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        placeholder="Username"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                        value={name}
                        onChange={handleChange}
                    />
                </InputGroup>
            </Row>
        </>
     );
}
 
export default UsersFilter;