
## PROJECT DESCRIPTION

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Purpose
  Project was designed to filter and get ids of chosen users

### Features

- filtering users by firs name or last name no matter of uppercase or lowercase
- view of users ids in console only if user have checked checkbox, console is rendering only when state of "ids" is changing
- list of users is rendering in organised alphabetically by last name

in addition
- app is rendering only a few of contacts set by "range" state
- posible of choose how many record can be rendered by seting new "range" state

### Packages I have chosen:
* Axios
* Bootstrap
* ContextApi
* Hooks(useContext, useState, useEffect, useReducer)

## Requirements

Node.js v 12.16.1 or higher

## How to run locally?

### Steps
1. `git clone https://gitlab.com/lysakowskimichal/teacode.git`

2. `cd teacode`

3. `npm i`

4. `npm start`

## License 

[MIT](https://opensource.org/licenses/MIT)